/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.geometry;

/**
 *
 * @author oscar
 */
public class geometry {
/**
* @param args the command line arguments
*/
    public static void main(String[] args) {
        // TODO code application logic here
        punto p = new punto(5,4);
        punto p1 = new punto(p);
        punto p2 = new punto();
        punto p3 = new punto(6,5);
        p2.x = 10;
        p2.y = 5;
        p.coord();
        p1.coord();
        p2.coord();
        linea l = new linea(p, p2);
        l.coord();
        l.longitud();
        circulo c = new circulo(p3, 2);
        c.coord();
        c.area();
        c.circunferencia();
    }
}