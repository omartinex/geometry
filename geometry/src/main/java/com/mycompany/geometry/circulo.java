/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.geometry;

/**
 *
 * @author oscar
 */
public class circulo {
    punto p3;
    double radio, circ, area;
    circulo(){
    }
    circulo(circulo c1){
        this.p3 = c1.p3;
        this.radio = c1.radio;
    }
    circulo(punto p3, double r){
        this.p3 = p3;
        this.radio = r;
    }

    void circunferencia(){
        circ = 2 * Math.PI * radio ;
        System.out.println("[+]La longitud de la circunferencia es: " + circ);
    }
    void area(){
        area = Math.PI * Math.pow(radio, 2);
        System.out.println("[+]El área del circulo es: " + area);
    }
    void coord(){
        System.out.println("\n[+]El centro del circulo está en las coordenadas: (" + p3.x + "," + p3.y + ")");
    }

    public static void main(String args[]){
        
    }
}
