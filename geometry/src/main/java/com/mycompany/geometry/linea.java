/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.geometry;

/**
 *
 * @author oscar
 */
public class linea {
    punto p1;
    punto p2;
    // double x1, x2;
    // double y1, y2;

    linea(){
    }
    
    linea(linea l1){
        this.p1 = l1.p1;
        this.p2 = l1.p2;
    }
    
    linea(punto p1, punto p2){
        this.p1 = p1;
        this.p2 = p2;
    }
    
    void coord(){
        System.out.println("\n[+]La línea pasa por los puntos: (" + p1.x + "," + p1.y + ") " + " ("+ p2.x + "," + p2.y + ")");
    }
    
    void longitud(){
        double resX, resY, op, l, xp, yp;
        resX = (p2.x - p1.x);
        //System.out.println(resX);
        xp = Math.pow(resX, 2);
        resY = (p2.y - p1.y);
        //System.out.println(resY);

        yp = Math.pow(resY, 2);
        l = Math.sqrt(xp + yp);
        System.out.println("[+]La longitud de la línea es: " + l + "\n");
    }
    
    public static void main(String args[]){
        
    }
}
